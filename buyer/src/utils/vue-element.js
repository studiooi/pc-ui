import Vue from 'vue'
import { Button, Skeleton, SkeletonItem } from 'element-ui'

Vue.use(Button).use(Skeleton).use(SkeletonItem)
