const { im, distribution } = require('./index')

module.exports = {
	NODE_ENV: '"test"',
	ENV_CONFIG: '"test"',
  IM: im,
	DISTRIBUTION: distribution
}
